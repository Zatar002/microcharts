﻿using System.Collections.Generic;
using Xamarin.Forms;

using SkiaSharp;
using Microcharts;

using Entry = Microcharts.Entry; //Entry should add Microsharts.Entry (instead of Xamarin's default) May be a good idea to change the name of this, to avoid confusion
using System.Linq;  //for FirstOrDefault to work

namespace hw6
{
    public partial class MainPage : ContentPage
    {
        public BarChart barChart;
        public DonutChart donutChart;
        public LineChart lineChart;

        public MainPage()
        {
            InitializeComponent();
            CreateCharts();
            FillPicker();
        }

        private void CreateCharts()
        {
            var entryList = new List<Entry>()
            {
                 new Entry(5)
                {
                    Label = "Timeline",
                    ValueLabel = "",
                    Color = SKColor.Parse("#00BFF3"),
                    
                },
                new Entry(5)
                {
                    Label = "Photos",
                    ValueLabel = "",
                    Color = SKColor.Parse("#FF3333"),
                },
                new Entry(5)
                {
                    Label = "Notifications",
                    ValueLabel = "",
                    Color = SKColor.Parse("#3333FF"),
                },
                new Entry(85)
                {
                    Label = "Shaking My Head",
                    ValueLabel = "",
                    Color = SKColor.Parse("#FF8800"),
                }
            };

            donutChart = new DonutChart() { Entries = entryList };
            donutChart.LabelTextSize = 30;

            barChart = new BarChart() { Entries = entryList };
            barChart.LabelTextSize = 30;

            lineChart = new LineChart() { Entries = entryList };
            lineChart.LabelTextSize = 30;
        }

        private void Handle_ChartPickerSelectedIndexChanged(object sender, System.EventArgs e)
        {
            var chosenChart = ChartPicker.Items[ChartPicker.SelectedIndex];
            switch (chosenChart)
            {
                case "Pie Chart":
                    Chart1.Chart = donutChart;
                    break;
                case "Bar Chart":
                    Chart1.Chart = barChart;
                    break;
                case "Line Chart":
                    Chart1.Chart = lineChart;
                    break;
            }
        }

        private void FillPicker()
        {
            ChartPicker.Items.Add("Pie Chart");
            ChartPicker.Items.Add("Bar Chart");
            ChartPicker.Items.Add("Line Chart");
            ChartPicker.SelectedItem = ChartPicker.Items.FirstOrDefault();
        }
    }
}
